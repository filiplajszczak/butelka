from datetime import datetime
from django.views.generic import TemplateView, CreateView, ListView
from django.urls import reverse_lazy
from django.utils.timezone import now
from datetime import timedelta
from django.db.models import Sum
from .forms import RecordForm
from .models import Feed
from .models import Food
from .forms import RecordNonForm

from excel_response import ExcelView


class FeedExportView(ExcelView):
    timestamp = datetime.now().strftime('%Y%m%d_%H_%M_%S')
    output_filename = 'Feed_{timestamp}'.format(timestamp=timestamp)
    model = Feed


class FoodExportView(ExcelView):
    timestamp = datetime.now().strftime('%Y%m%d_%H_%M_%S')
    output_filename = 'Food_{timestamp}'.format(timestamp=timestamp)
    model = Food


class DownloadView(TemplateView):
    template_name = 'milk/download.html'


class HomeView(TemplateView):

    """General status and available options.

    Time till next planned feeding (3h between feedings over 60 ml of milk)
    Progressbar till next feeding

    Choice of available options.

    - Record feeding
    - Check sum of feedings

    Information about last feeding regardless the quantity of milk

    """

    template_name = 'milk/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        try:
            last_feed = Feed.objects.latest('time')
            last_food = Food.objects.latest('time')
            hungry_seconds = (now() - last_feed.time).total_seconds()
            hungry_hours = int(hungry_seconds / 3600)
            hungry_minutes = int((hungry_seconds % 3600) / 60)
            last_big_feed = Feed.objects.filter(milk__gte=60).latest('time')
            minutes_since_last_big_feed = int((now() - last_big_feed.time).total_seconds() / 60)
            till_next_feed = 360 - minutes_since_last_big_feed
            percent = int(minutes_since_last_big_feed / 3.60)
        except Feed.DoesNotExist:
            last_feed = None
            last_food = None
            hungry_hours = '-'
            hungry_minutes = '-'
            hungry_seconds = 0
            till_next_feed = '-'
            percent = 0

        context['last_feed'] = last_feed
        context['last_food'] = last_food
        context['hungry_hours'] = hungry_hours
        context['hungry_minutes'] = hungry_minutes
        context['hungry_seconds'] = hungry_seconds
        context['minutes'] = till_next_feed
        cap = lambda x: x if x < 100 else 100
        context['bar'] = cap(percent)
        return context


class RecordView(CreateView):

    """Form gathers data about feeding

    - Who was feeding? (optional)
    - When? (automatically filled with current date and time)
    - How much milk? (in milliliters)

    """

    template_name = 'milk/record.html'
    form_class = RecordForm
    success_url = reverse_lazy('milk:home')

    def get_initial(self):
        return {'time': now()}

class RecordViewNon(CreateView):

    """Form gathers data about feeding

    - Who was feeding? (optional)
    - When? (automatically filled with current date and time)
    - How much milk? (in milliliters)

    """

    template_name = 'milk/recordnon.html'
    form_class = RecordNonForm
    success_url = reverse_lazy('milk:home')

    def get_initial(self):
        return {'time': now()}

class CheckTotalView(ListView):

    """Overview report

    - Information about quantity of milk consumed on the current day
    - Information about consumption on previous day
    - List of recent feedings

    """

    template_name = 'milk/check_total.html'
    queryset = Feed.objects.order_by('-time')[:10]

    def get_context_data(self, **kwargs):
        context = super(CheckTotalView, self).get_context_data(**kwargs)
        context['feed_total'] = Feed.objects.filter(time__date=now().today()).aggregate(Sum('milk'))
        context['feed_yesterday'] = Feed.objects.filter(time__date=now().today() - timedelta(days=1)
                                                        ).aggregate(Sum('milk'))
        context['other_food'] = Food.objects.order_by('-time')[:10]
        context['food_yesterday'] = Food.objects.filter(time__date=now().today() - timedelta(days=1)
                                                        ).aggregate(Sum('foods'))

        context['food_total'] = Food.objects.filter(time__date=now().today()).aggregate(Sum('foods'))

        print(context)
        return context
