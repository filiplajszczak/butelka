from django.urls import path
from .views import HomeView, RecordView, RecordViewNon, CheckTotalView, FeedExportView, FoodExportView, DownloadView

app_name = 'milk'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('record/', RecordView.as_view(), name='record'),
    path('recordnon/', RecordViewNon.as_view(), name='recordnon'),
    path('check_total/', CheckTotalView.as_view(), name='check_total'),
    path('food/', FoodExportView.as_view(), name='food'),
    path('feed/', FeedExportView.as_view(), name='feed'),
    path('download/', DownloadView.as_view(), name='download'),
]