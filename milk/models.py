from django.db import models


class Feed(models.Model):
    feeder = models.CharField(blank=True, max_length=1, choices=(('', 'Kto karmił?'), ('M', 'Mama'), ('T', 'Tata'), ('O', 'Ktoś inny')))
    time = models.DateTimeField()
    milk = models.IntegerField()

    def __str__(self):
        return f'Nakarmiona o {self.time:%H}:{self.time:%M} {self.time:%d}-{self.time:%m}-{self.time:%Y} ' \
               f'{self.milk} ml mleka przez {self.feeder}'


class Food(models.Model):
    feeder = models.CharField(blank=True, max_length=1, choices=(('', 'Kto karmił?'), ('M', 'Mama'), ('T', 'Tata'), ('O', 'Ktoś inny')))
    type = models.CharField(blank=True, max_length=50)
    factory = models.CharField(blank=True, max_length=50)
    time = models.DateTimeField()
    foods = models.IntegerField()

    def __str__(self):
        return f'Nakarmiona o {self.time:%H}:{self.time:%M} {self.time:%d}-{self.time:%m}-{self.time:%Y} ' \
               f'{self.foods} g {self.type} marki {self.factory} przez {self.feeder}'
