from django import forms
from .models import Feed
from .models import Food

class RecordForm(forms.ModelForm):
    class Meta:
        model = Feed
        fields = ['feeder', 'time', 'milk']
        labels = {'feeder': '', 'time': 'Kiedy?', 'milk': 'Ile mililitrów mleka?'}

class RecordNonForm(forms.ModelForm):
    class Meta:
        model = Food
        fields = ['feeder', 'time', 'foods', 'factory', 'type']
        labels = {'feeder': '', 'time': 'Kiedy?', 'foods': 'Ile gramów jedzenia?', 'factory': 'jakiej firmy?', 'type': 'co zjadła? (np. kaszkę albo kurczaczka z ziemniaczkami)'}